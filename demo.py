#!/usr/bin/env python

# program description: demonstration of python(v2.7.6)/boto(v2.36.0)/aws;
# written by c.j. sperber on 1/31/2015

import collections
import re

import boto

print "Program description: python(v2.7.6)/boto(v2.36.0)/aws demonstration\n"

# using AWS credentials located within environmental variables
# AWS_SECRET_KEY_ID and AWS_SECRET_ACCESS_KEY
ec2 = boto.connect_ec2()


# availability zones
#aws_availability_zones = ec2.get_all_zones()

#print "AWS availability zones:"
#for zone in aws_availability_zones:
#    print "Zone: '%s'" % zone
#print


# regions particular to this ec2 instance
#ec2_regions = boto.ec2.regions()

#print "EC2 regions:"
#for region in ec2_regions:
#    print "Region: '%s'" % region
#print


# security groups particular to this ec2 instance
security_groups = ec2.get_all_security_groups()

print "There are %s security group%s associated with this EC2 instance.\n" % \
    (len(security_groups), '' if len(security_groups) == 1 else 's') 


for ctr, security_group in enumerate(security_groups):
    # default value of new key w/o value is an empty list;
    # callable required as first parameter
    protocol_ingress = collections.defaultdict(lambda:[])

    print "Security group: '%s'\n" % security_group
    
    for property, value in vars(security_groups[ctr]).iteritems():
        value_as_string = str(value).strip()
        print "%-*s : %s" % (20, property, \
            value_as_string if len(value_as_string) > 0 else 'Not set/empty')

    print "\nProtocol rules in specificity:"
    for rule in security_group.rules:
        #print rule

        match_proto = re.search(r'(\w+(?=\())', str(rule))
        match_start_port = re.search(r'(\d+(?=\-))', str(rule))
        match_end_port = re.search(r'(\d+(?=\)))', str(rule))
       
        protocol = match_proto.group(1)
        port_set = match_start_port.group(1) + " - " + match_end_port.group(1) if \
            match_start_port.group(1) != match_end_port.group(1) else \
            match_start_port.group(1)

        print "protocol: %s, port(s): %s" % (protocol, port_set)
    
        protocol_ingress[protocol].append(port_set)

    for protocol in protocol_ingress:
        print "\n(parsed): %s: " % protocol,
        
        port_sets = sorted(set(protocol_ingress[protocol]), key=lambda p: int(p.split(' ')[0]))

        for ctr, port_set in enumerate(port_sets):
            if ctr+1 != len(port_sets):
                print "%s, " % port_set,
            else:
                print "%s" % port_set,
    print

    if ctr+1 != len(security_groups):
        print "\n%s" % "**********"
 
    print
